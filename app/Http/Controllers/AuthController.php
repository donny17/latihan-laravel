<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg() 
    {
        return view('register');
    }

    public function wel(Request $request)
    {
    //    dd($request->all());
        $namadepan = $request['nama_depan'];
        $namabelakang = $request['nama_belakang'];

        return view('welcome', compact("namadepan", "namabelakang"));
    }
}
