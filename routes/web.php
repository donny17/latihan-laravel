<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  'IndexController@home');
Route::get('/register',  'AuthController@reg');
Route::post('/welcome',  'AuthController@wel');
Route::get('/data-table',  'IndexController@table');


// CRUD cast
// create
Route::get('/cast/create', 'CastController@create'); //mengarah ke form tambah data
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table cast


//Read
Route::get('/cast', 'CastController@index');//ambil data kdr data base, lalu di tampilkan
Route::get('/cast/{cast_id}', '	CastController@show' ); //route detail cast