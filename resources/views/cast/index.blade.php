@extends('layout.master')
@section('title')
List Nama Pemain Film
@endsection
@section('content')

<a href="/cast/create" class="btn ntn-primary mb-3">Tambah Nama Pemain Film</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item ->nama}}</td>
            <td>{{$item ->umur}}</td>
            <td>{{$item ->bio}}</td>
            <td>
                <a href="/cast/{{$item->id}}" class="btn ntn-primary mb-3">Detail</a>
            </td>
        </tr>
            
        @empty
            <h1>Data Tidak Ada</h1>
        @endforelse

    </tbody>
  </table>



@endsection