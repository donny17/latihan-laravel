@extends('layout.master')
@section('title')
Halaman Pendaftaran
@endsection
@section('content')
<h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <label> First name : </label> <br> <br>
        <input type="text" name="nama_depan" > <br> <br>
        <label> Last name : </label> <br> <br>
        <input type="text" name="nama_belakang">  <br><br>
        <label> Gender</label> <br> <br>
        <input type="radio" name="Gr"> Male <br>
        <input type="radio" name="Gr"> Female <br>
        <input type="radio" name="Gr"> Other <br> <br>
        <label>Nationality</label> <br> <br>
        <select name="Nationality" id="">
            <option value="1">Indonesia</option> 
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select> <br> <br>
        <label>Language Spoken</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br> <br>
        <label>Bio</label> <br> <br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up">
@endsection